﻿#pragma once



#include "CoreMinimal.h"
#include "Engine/Blueprint.h"
#include "Tests/AutomationCommon.h"
#include "TPSTypes.generated.h"

UENUM(BlueprintType)
enum class EInventoryItemType: uint8
{
	SPHERE = 0 UMETA(DisplayName = "MY COOL SPHERE"),
	CUBE UMETA(DisplayName = "MY COOL CUBE"),
	CYLINDER,
	CONE
};

USTRUCT(BlueprintType)
struct FInventoryData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EInventoryItemType Type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0"))
	int32 Score;
};


#define ENUM_LOOP_START(TYPE, EnumElem)                                    \
for (int32 Index = 0; Index < StaticEnum<TYPE>()->NumEnums() - 1; ++Index) \
{                                                                          \
const auto EnumElem = static_cast<TYPE>(StaticEnum<TYPE>()->GetValueByIndex(Index));
#define ENUM_LOOP_END }

template<typename T>
	T* CreateBlueprint(UWorld* World, const FString& Name, const FTransform& Transform = FTransform::Identity)
{
	const UBlueprint* Blueprint = LoadObject<UBlueprint>(nullptr, *Name);
	return (World && Blueprint) ? World->SpawnActor<T>(Blueprint->GeneratedClass,Transform ) : nullptr;

}

class LevelScope
{
public:
	LevelScope(const FString& MapName){AutomationOpenMap(MapName);}
	~LevelScope(){ADD_LATENT_AUTOMATION_COMMAND(FExitGameCommand);}
};

UWorld* GetTestGameWorld();

