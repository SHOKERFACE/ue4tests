#if WITH_AUTOMATION_TESTS

#include "CoreMinimal.h"
#include "Misc/AutomationTest.h"
#include "UnitTests/TPSInventoryComponentTest.h"
#include "Components/TPSInventoryComponent.h"
#include "TPSTypes.h"

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FComponentCouldBeCreated, "TestGame.Components.Inventory.ComponentCouldBeCreated",
                                 EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter |
                                 EAutomationTestFlags::HighPriority)

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FItemScoresShouldBeZeroesByDefault,
                                 "TestGame.Components.Inventory.ItemScoresShouldBeZeroesByDefault",
                                 EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter |
                                 EAutomationTestFlags::HighPriority)

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FNegativeScoreShouldntBeAdded,
                                 "TestGame.Components.Inventory.NegativeScoreShouldntBeAdded",
                                 EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter |
                                 EAutomationTestFlags::HighPriority)

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FPositiveScoreShouldBeAdded,
                                 "TestGame.Components.Inventory.PositiveScoreShouldBeAdded",
                                 EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter |
                                 EAutomationTestFlags::HighPriority)

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FScoreThanLimitCantBeAdded, "TestGame.Components.Inventory.ScoreThanLimitCantBeAdded",
                                 EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter |
                                 EAutomationTestFlags::HighPriority)


namespace
{
	class UTPSInventoryComponentTestable : public UTPSInventoryComponent
	{
	public:
		void SetLimits(const TMap<EInventoryItemType, int32>& Limits) { InventoryLimits = Limits; };
	};

	TMap<EInventoryItemType, int32> InitLimits(UTPSInventoryComponentTestable* InvComp, int32 LimitValue)
	{
		TMap<EInventoryItemType, int32> InvLimits;
		ENUM_LOOP_START(EInventoryItemType, EElem)
			InvLimits.Add(EElem, LimitValue);
		ENUM_LOOP_END
		InvComp->SetLimits(InvLimits);

		return InvLimits;
	}
}

bool FComponentCouldBeCreated::RunTest(const FString& Parameters)
{
	const UTPSInventoryComponent* InventoryComp = NewObject<UTPSInventoryComponent>();
	if (!TestNotNull("Inventory component exists", InventoryComp)) return false;

	return true;
}

bool FItemScoresShouldBeZeroesByDefault::RunTest(const FString& Parameters)
{
	const UTPSInventoryComponent* InventoryComp = NewObject<UTPSInventoryComponent>();
	if (!TestNotNull("Inventory component exists", InventoryComp)) return false;


	ENUM_LOOP_START(EInventoryItemType, EElem)
		TestTrueExpr(InventoryComp->GetInventoryAmountByType(EElem) == 0);
	ENUM_LOOP_END
	return true;
}

bool FNegativeScoreShouldntBeAdded::RunTest(const FString& Parameters)
{
	auto InventoryComponent = NewObject<UTPSInventoryComponentTestable>();
	if (!TestNotNull("InventoryComponentExist", InventoryComponent)) return false;

	InitLimits(InventoryComponent, 100);

	const int32 NegativeScoreAmount = - 5;
	const int32 InitialScoreAmount = 10;
	ENUM_LOOP_START(EInventoryItemType, EElem)
		TestTrueExpr(InventoryComponent->TryToAddItem({EElem, InitialScoreAmount}));
		TestTrueExpr(InventoryComponent->GetInventoryAmountByType(EElem) == InitialScoreAmount);
		TestTrueExpr(!InventoryComponent->TryToAddItem({EElem, NegativeScoreAmount}));
		TestTrueExpr(InventoryComponent->GetInventoryAmountByType(EElem) == InitialScoreAmount);
	ENUM_LOOP_END

	return true;
}

bool FPositiveScoreShouldBeAdded::RunTest(const FString& Parameters)
{
	auto InventoryComponent = NewObject<UTPSInventoryComponentTestable>();
	if (!TestNotNull("InventoryComponentExist", InventoryComponent)) return false;


	const auto InvLimits = InitLimits(InventoryComponent, 100);

	ENUM_LOOP_START(EInventoryItemType, EElem)
		for (int32 i = 0; i < InvLimits[EElem]; ++i)
		{
			TestTrueExpr(InventoryComponent->TryToAddItem({EElem, 1}));
			TestTrueExpr(InventoryComponent->GetInventoryAmountByType(EElem) == i + 1);
		}
	ENUM_LOOP_END

	return true;
}

bool FScoreThanLimitCantBeAdded::RunTest(const FString& Parameters)
{
	auto InventoryComponent = NewObject<UTPSInventoryComponentTestable>();
	if (!TestNotNull("InventoryComponentExist", InventoryComponent)) return false;

	const int32 ScoreLimit = 100;
	InitLimits(InventoryComponent, ScoreLimit);
	TestTrueExpr(InventoryComponent->TryToAddItem({EInventoryItemType::SPHERE, 10}));
	TestTrueExpr(InventoryComponent->GetInventoryAmountByType({EInventoryItemType::SPHERE}) == 10);

	TestTrueExpr(!InventoryComponent->TryToAddItem({EInventoryItemType::SPHERE, ScoreLimit + 1}));
	TestTrueExpr(InventoryComponent->GetInventoryAmountByType({EInventoryItemType::SPHERE}) == 10);


	return true;
}
#endif
