
#if WITH_AUTOMATION_TESTS

#include "CoreMinimal.h"
#include "Misc/AutomationTest.h"
#include "UnitTests/TPSInventoryItemTests.h"
#include "Test/Items/TPSInventoryItem.h"
#include "Engine/World.h"
#include "Components/TextRenderComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "TPSTypes.h"

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FCppActorCantBeCreated, "TestGame.Items.Inventory.CppActorCantBeCreated",
								EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter |
								EAutomationTestFlags::HighPriority)

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FBlueprintShouldBeSetupCorrectly, "TestGame.Items.Inventory.BlueprintShouldBeSetupCorrectly",
								EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter |
								EAutomationTestFlags::HighPriority)

namespace
{
	const char* InventoryItemBPName = "Blueprint'/Game/Inventory/BP_TPSInventoryItem.BP_TPSInventoryItem'";

	

	

	
	
}

bool FCppActorCantBeCreated::RunTest(const FString& Parameters)
{
	const FString ExpectedWarnMsg = FString::Printf(TEXT("SpawnActor failed because class %s is abstract"), *ATPSInventoryItem::StaticClass()->GetName());
	AddExpectedError(ExpectedWarnMsg, EAutomationExpectedErrorFlags::Exact);
	
	LevelScope("/Game/Tests/EmptyTestLevel");

	UWorld* World = GetTestGameWorld();
	if(!TestNotNull("World exists", World)) return false;

	const FTransform InitialTransform{FVector{1000}};
	const ATPSInventoryItem* InventoryItem = World->SpawnActor<ATPSInventoryItem>(ATPSInventoryItem::StaticClass(),InitialTransform );
	if(!TestNull("Inventory item exists", InventoryItem)) return false;
	
	return true;
	
}

bool FBlueprintShouldBeSetupCorrectly::RunTest(const FString& Parameters)
{
	LevelScope("/Game/Tests/EmptyTestLevel");
	UWorld* World = GetTestGameWorld();
	if(!TestNotNull("World exists", World)) return false;

	
	const FTransform InitialTransform{FVector{1000}};
	const ATPSInventoryItem* InventoryItem = CreateBlueprint<ATPSInventoryItem>(World, InventoryItemBPName);
	if(!TestNotNull("Inventory item exists", InventoryItem)) return false;
	
	const auto CollisionComp = InventoryItem->FindComponentByClass<USphereComponent>();

	TestTrueExpr(CollisionComp->GetUnscaledSphereRadius() >= 30.0f);
	TestTrueExpr(CollisionComp->GetCollisionEnabled() == ECollisionEnabled::QueryOnly);
	TestTrueExpr(CollisionComp->GetGenerateOverlapEvents());
	TestTrueExpr(InventoryItem->GetRootComponent() == CollisionComp);

	ENUM_LOOP_START(ECollisionChannel, EElem)
	if(EElem != ECC_OverlapAll_Deprecated)
	{
		TestTrueExpr(CollisionComp->GetCollisionResponseToChannel(EElem) == ECR_Overlap);
	}
	ENUM_LOOP_END

	const auto TextRenderComp = InventoryItem->FindComponentByClass<UTextRenderComponent>();
	if(!TestNotNull("Text Render Component exists", TextRenderComp)) return false;

	const auto StaticMeshComp = InventoryItem->FindComponentByClass<UStaticMeshComponent>();
	if(!TestNotNull("Static Mesh Component exists", TextRenderComp)) return false;

	TestTrueExpr(StaticMeshComp->GetCollisionEnabled() == ECollisionEnabled::NoCollision);

	return true;
}

#endif
