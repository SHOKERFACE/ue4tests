﻿

#include "TPSTypes.h"

UWorld* GetTestGameWorld()
{
	{
		const TIndirectArray<FWorldContext>& WorldContexts = GEngine->GetWorldContexts();
		for ( const FWorldContext& Context : WorldContexts )
		{
			if ( (  Context.WorldType == EWorldType::PIE ||  Context.WorldType == EWorldType::Game )  &&  Context.World())
			{
				return Context.World();
			}
		}
		return nullptr;
	}
}

